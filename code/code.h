/*
  Different functions that compute the value of
  the polynomial 

        2
      ax + bx + c

  for a given values of a, b, c, and x.
*/

#ifndef EVAL
#define EVAL

double polyEval1(int a, int b, int c, double x);
double polyEval2(int a, int b, int c, double x);
double polyEval3(int coefficients[], double x);
double polyEval4(int coefficients[], double x);

#endif
