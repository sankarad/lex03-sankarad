#include <stdio.h>
#include "CUnit.h"
#include "Basic.h"
#include "code.h"

static double epsilon = 0.000001;

void runTest1(int,int,int,double,double);
void runTest2(int,int,int,double,double);
void runTest3(int,int,int,double,double);
void runTest4(int,int,int,double,double);

void test11(void) { runTest1(2,3,5,1,10); }
void test12(void) { runTest2(2,3,5,1,10); }
void test13(void) { runTest3(2,3,5,1,10); }
void test14(void) { runTest4(2,3,5,1,10); }

void runTest1(int a, int b, int c, double x, double expected) {
  double actual = polyEval1(a,b,c,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}
void runTest2(int a, int b, int c, double x, double expected) {
  double actual = polyEval2(a,b,c,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}
void runTest3(int a, int b, int c, double x, double expected) {
  int coeff[] = {a,b,c};
  double actual = polyEval3(coeff,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}
void runTest4(int a, int b, int c, double x, double expected) {
  int coeff[] = {a,b,c};
  double actual = polyEval4(coeff,x);
  CU_ASSERT_DOUBLE_EQUAL( actual, expected, epsilon );
}

/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main()
{
   CU_pSuite pSuite = NULL;

   /* initialize the CUnit test registry */
   if (CUE_SUCCESS != CU_initialize_registry()) { return CU_get_error(); }

   /* add a suite to the registry */
   pSuite = CU_add_suite("Suite_1", NULL, NULL);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* add the tests to the suite */
   if (
          (NULL == CU_add_test(pSuite, "test11", test11))
       || (NULL == CU_add_test(pSuite, "test12", test12)) 
       || (NULL == CU_add_test(pSuite, "test13", test13)) 
       || (NULL == CU_add_test(pSuite, "test14", test14)) 

      )
   {
      CU_cleanup_registry();
      return CU_get_error();
   }

   /* Run all tests using the CUnit Basic interface */
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   //   CU_basic_show_failures(CU_get_failure_list());
   CU_cleanup_registry();
   return CU_get_error();
}
